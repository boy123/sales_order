<?php 
$so_h = $this->db->get_where('so_header', ['so_no' => $this->input->get('so_no') ])->row();

 ?>

<div class="card">
     <div class="card-body">
          <div class="row mb-4">
               <div class="col-sm-8">
                    <table class="table table-bordered">
                         <tr>
                              <th>Company Code</th>
                              <td colspan="3">
                                   <select name="company_code" id="company_code" class="single-select form-control form-control-sm" onchange="cekAccNo()" disabled>
                                        <?php 
                                        $id_user = $this->session->userdata('id_user');
                                        $acc_no = get_data('app_user','id_user',$id_user,'acc_no');
                                        if ($this->session->userdata('level') == 'sales') {
                                             $where = "WHERE SalesAgent IN (select sales_agent from user_agent where id_user='$id_user') ";
                                        } elseif($this->session->userdata('level') == 'other') {
                                             $where = "WHERE AccNo = '$acc_no' ";
                                        } else {
                                             $where = "";
                                        }
                                        $sql = "SELECT AccNo, CompanyName FROM debtor $where";
                                        $debtor = $this->db->query($sql);
                                        foreach ($debtor->result() as $rw): ?>
                                             <?php if ($rw->AccNo == $so_h->acc_no): ?>
                                                  <option value="<?php echo $rw->AccNo ?>" selected><?php echo $rw->AccNo.' | '.$rw->CompanyName ?></option>
                                             <?php endif ?>
                                             <option value="<?php echo $rw->AccNo ?>"><?php echo $rw->AccNo.' | '.$rw->CompanyName ?></option>
                                        <?php endforeach ?>
                                   </select>
                              </td>
                         </tr>
                         <tr>
                              <th>Company Name</th>
                              <td colspan="3">
                                   <input type="text" class="form-control form-control-sm" name="CompanyName" id="CompanyName" readonly>
                              </td>
                         </tr>
                         <tr>
                              <th>Address</th>
                              <td>
                                   <ul class="nav nav-tabs nav-default" role="tablist">
                                        <li class="nav-item" role="presentation">
                                             <a class="nav-link active" data-bs-toggle="tab" href="#primaryhome" role="tab" aria-selected="true">
                                                  <div class="d-flex align-items-center">
                                                       <div class="tab-icon"><i class='bx bx-home font-18 me-1'></i>
                                                       </div>
                                                       <div class="tab-title">Address</div>
                                                  </div>
                                             </a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                             <a class="nav-link" data-bs-toggle="tab" href="#primaryprofile" role="tab" aria-selected="false">
                                                  <div class="d-flex align-items-center">
                                                       <div class="tab-icon"><i class='bx bx-user-pin font-18 me-1'></i>
                                                       </div>
                                                       <div class="tab-title">Delevery Address</div>
                                                  </div>
                                             </a>
                                        </li>
                                   </ul>
                                   <div class="tab-content py-3">
                                        <div class="tab-pane fade show active" id="primaryhome" role="tabpanel">
                                             <input type="text" class="form-control form-control-sm" name="Address1" id="Address1" readonly>
                                             <input type="text" class="form-control form-control-sm" name="Address2" id="Address2" readonly>
                                             <input type="text" class="form-control form-control-sm" name="Address3" id="Address3" readonly>
                                             <input type="text" class="form-control form-control-sm" name="Address4" id="Address4" readonly>
                                        </div>
                                        <div class="tab-pane fade" id="primaryprofile" role="tabpanel">
                                             <input type="text" class="form-control form-control-sm" name="DeliverAddr1" value="<?php echo $so_h->delivery1 ?>" id="DeliverAddr1" readonly>
                                             <input type="text" class="form-control form-control-sm" name="DeliverAddr2" value="<?php echo $so_h->delivery2 ?>" id="DeliverAddr2" readonly>
                                             <input type="text" class="form-control form-control-sm" name="DeliverAddr3" value="<?php echo $so_h->delivery3 ?>" id="DeliverAddr3" readonly>
                                             <input type="text" class="form-control form-control-sm" name="DeliverAddr4" value="<?php echo $so_h->delivery4 ?>" id="DeliverAddr4" readonly>
                                        </div>
                                   </div>
                              </td>
                         </tr>
                         <tr>
                              <th>Branch</th>
                              <td colspan="3">
                                   <select name="branch" id="branch" class="single-select" style="width: 100%" disabled>
                                        <option value="<?php echo $so_h->branch ?>"><?php echo get_data('branch','BranchCode',$so_h->branch,'BranchName') ?></option>
                                   </select>
                              </td>
                         </tr>
                    </table>
               </div>
               <div class="col-sm-4">
                    <table class="table table-bordered">
                         <tr>
                              <th>S/O No</th>
                              <td>
                                   <b><?php echo $this->input->get('so_no') ?></b>
                              </td>
                         </tr>
                         <tr>
                              <th>Date</th>
                              <td>
                                   <input type="date" value="<?php echo $so_h->date ?>" class="form-control form-control-sm" name="date" id="date" readonly>
                              </td>
                         </tr>
                         <tr>
                              <th>Credit Term</th>
                              <td>
                                   <input type="text" class="form-control form-control-sm" name="DisplayTerm" id="DisplayTerm" readonly>
                              </td>
                         </tr>
                         <tr>
                              <th>Sales Agent</th>
                              <td>
                                   <input type="text" class="form-control form-control-sm" name="SalesAgent" id="SalesAgent" value="<?php echo $so_h->sales_agent ?>" readonly>
                              </td>
                         </tr>
                         <tr>
                              <th>Ref Doc No</th>
                              <td>
                                   <input type="text" class="form-control form-control-sm" name="RefDocNo" value="<?php echo $so_h->ref_doc ?>" id="RefDocNo" readonly>
                              </td>
                         </tr>
                    </table>
               </div>
          </div>
          <div class="row mb-3">
               <div class="col">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
               </div>
          </div>
          <div class="table-responsive">
              
               <table class="table table-striped table-bordered" style="width:100%">
                    <thead>
                         <tr>
                              <th>Item Code</th>
                              <th>Item Description</th>
                              <th>Qty</th>
                              <th>UOM/Satuan</th>
                              <th>Unit Price</th>
                              <th>Discount</th>
                              <th>Subtotal</th>
                              <th>PPN Code</th>
                              <th>PPN Rate (%)</th>
                              <th>PPN Amount</th>
                              <th>Proj No</th>
                         </tr>
                    </thead>
                    <tbody id="DetailItem">
                    </tbody>
                    <tfoot>
                         
                         <tr>
                              <td colspan="2"></td>
                              <td class="text-end"><span id="totQty">0</span></td>
                              <td colspan="3"></td>
                              <td class="text-end"><span id="totSubtotal">0</span></td>
                              <td colspan="2"></td>
                              <td class="text-end"><span id="totPPN">0</span></td>
                              <td colspan="2"></td>
                         </tr>
                         <tr>
                              <td colspan="10" class="text-end">Subtotal(ex)</td>
                              <td colspan="2" class="text-end"><span id="footSubtotal">0</span></td>
                         </tr>
                         <tr>
                              <td colspan="8" class="text-end">Taxable Amount</td>
                              <td class="text-end"><span id="footTaxable">0</span></td>
                              <td class="text-end">PPN</td>
                              <td colspan="2" class="text-end"><span id="footPPN">0</span></td>
                         </tr>
                         <tr>
                              <td colspan="4" class="text-end">Currency</td>
                              <td ><span id="footCurrency">IDR</span></td>
                              <td class="text-end">Rate</td>
                              <td class="text-end"><span id="footRate">0</span></td>
                              <td class="text-end">Local Total</td>
                              <td class="text-end"><span id="footLocal">0</span></td>
                              <td class="text-end">Total</td>
                              <td colspan="2" class="text-end"><span id="footTotal">0</span></td>
                         </tr>
                    </tfoot>
               </table>

          </div>
          
     </div>
</div>


<script type="text/javascript">

     $(document).ready(function() {
          cekAccNo();
     });

     var AccNo = "";
     var totQty = 0;
     var totSubtotal = 0;
     var totPPN = 0;
     var footRate = 0;
     var currency_code = 'IDR';
     var local_ppn = 0;
     var total = 0;
     var local = 0;

     var is_detail_item = "<?php echo is_detail_item() ?>";

     // Create our number formatter.
     var formatter = new Intl.NumberFormat('en-US');

     // loadDetailItem();
     
     $("#qty").on('keypress',function(e) {
         if(e.which == 13) {
             alert('You pressed enter!');
         }
     });

     function cekAccNo() {
          AccNo = $('#company_code').val();
          $.ajax({
               type: "GET",
               url: "sales_order/cekAccNo/"+AccNo,
               dataType: "json",
               success: function (response) {
                    $("#CompanyName").val(response.CompanyName);
                    $("#Address1").val(response.Address1);
                    $("#Address2").val(response.Address2);
                    $("#Address3").val(response.Address3);
                    $("#Address4").val(response.Address4);
                    $("#DeliverAddr1").val(response.DeliverAddr1);
                    $("#DeliverAddr2").val(response.DeliverAddr2);
                    $("#DeliverAddr3").val(response.DeliverAddr3);
                    $("#DeliverAddr4").val(response.DeliverAddr4);
                    $("#DisplayTerm").val(response.DisplayTerm);
                    $("#SalesAgent").val(response.SalesAgent);
                    getBranch(AccNo);
                    getCurr(AccNo);
                    loadDetailItem();
               }
          });
     }

     function getBranch(AccNo) {
          $.ajax({
               type: "GET",
               url: "sales_order/getBranch/"+AccNo,
               dataType: "html",
               success: function (response) {
                    $("#branch").html(response);
               }
          });
     }

     function showDetail() {
          $("#detailItem").show();
     }
     function hideDetail() {
          $("#detailItem").hide();
     }

     function cekUOM() {
          var ItemCode = $('#item_code').val();
          $.ajax({
               type: "GET",
               url: "sales_order/cekUOM/"+ItemCode,
               dataType: "html",
               success: function (response) {
                    $("#uom").html(response);
               }
          });
     }

     function saveItem() {
          var item_code = $("#item_code").val();
          var qty = $("#qty").val();
          var uom = $("#uom").val();

          if (AccNo == '') {
               alert("silahkan pilih Company Code dahulu !");
               empty_item();
               return;
          }
          

          if (item_code == '' || qty == '' || uom == '') {
               alert("masih data item yang kosong !");
               $('#uom option[value=]').attr('selected','selected');
               return;
          }

          $.ajax({
               type: "POST",
               url: "sales_order/saveItem/",
               data: {
                    item_code: $("#item_code").val(),
                    qty: $("#qty").val(),
                    uom: $("#uom").val(),
                    acc_no: AccNo
               },
               dataType: "json",
               success: function (response) {
                    console.log(response);
                    is_detail_item++;
                    loadDetailItem();
               }
          });
     }

     function loadDetailItem() {
          $.ajax({
               type: "GET",
               url: "sales_order/getListItem/",
               dataType: "json",
               success: function (data) {
                    $("#DetailItem").empty();
                    totQty = 0;
                    totSubtotal = 0;
                    totPPN = 0;
                    ppnAmount = 0;
                    var trHTML = '';
                    $.each(data.result, function (i, item) {

                      ppnAmount = ( parseFloat(item.subtotal) * parseInt(item.ppn_rate) ) / 100;
                      trHTML += '<tr>'
                                        +'<td>' + item.item_code + '</td>'
                                        +'<td>' + item.description + '</td>'
                                        +'<td class="text-end">' + item.qty.toFixed(2) + '</td>'
                                        +'<td>' + item.uom + '</td>'
                                        +'<td class="text-end">' + parseFloat(item.unit_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '</td>'
                                        +'<td class="text-end">' + parseFloat(item.discount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '</td>'
                                        +'<td class="text-end">' + parseFloat(item.subtotal).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '</td>'
                                        +'<td>' + item.ppn_code + '</td>'
                                        +'<td class="text-end">' + item.ppn_rate + '</td>'
                                        +'<td class="text-end">' + ppnAmount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + '</td>'
                                        +'<td>' + item.proj_no + '</td>'
                                 +'</tr>';

                              totQty += item.qty;
                              totSubtotal += item.subtotal;
                              totPPN += ppnAmount;
                    });

                    $('#DetailItem').append(trHTML);
                    $("#totQty").text(totQty.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    $("#totSubtotal").text(totSubtotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    $("#totPPN").text(totPPN.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

                    $("#footSubtotal").text(totSubtotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    $("#footTaxable").text(totSubtotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                   
                    local_ppn = parseInt(totPPN) * parseInt(footRate);
                    console.log(totPPN+" "+footRate);
                    console.log(local_ppn);
                    total = parseInt(totSubtotal) + parseInt(totPPN) ;
                    local = parseInt(total) * parseInt(footRate);
                     $("#footPPN").text(local_ppn.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    $("#footTotal").text(total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    $("#footLocal").text(local.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    empty_item();
               }
          });
     }

     function empty_item() {
          $("#item_code").val('').change();
          $("#qty").val('');
          $('#uom option[value=]').attr('selected','selected');
     }

     function hapusItem(rowid) {
          $.ajax({
               type: "GET",
               url: "sales_order/removeItem/"+rowid,
               dataType: "json",
               success: function (response) {
                    loadDetailItem();
               }
          });
     }

     function getCurr(acc_no) {
          $.ajax({
               type: "GET",
               url: "sales_order/getCurrency/"+acc_no,
               dataType: "json",
               success: function (response) {
                    currency_code = response.currency_code
                    $("#footCurrency").text(response.currency_code);
                    footRate = response.rate;
                    $("#footRate").text(response.rate);
               }
          });
     }

     function save(status) {
          if (is_detail_item == 0) {
               alert("Detail Transaksi tidak boleh kosong !");
               return;
          }

          $.ajax({
               type: "POST",
               url: "sales_order/updateSO/"+status+"?so_no=<?php echo $this->input->get('so_no') ?>",
               data: {
                    acc_no: $("#company_code").val(),
                    delivery1: $("#DeliverAddr1").val(),
                    delivery2: $("#DeliverAddr2").val(),
                    delivery3: $("#DeliverAddr3").val(),
                    delivery4: $("#DeliverAddr4").val(),
                    branch: $("#branch").val(),
                    so_no: $("#so_no").val(),
                    date: $("#date").val(), 
                    sales_agent: $("#SalesAgent").val(),
                    ref_doc: $("#RefDocNo").val(),

                    subtotal: totSubtotal,
                    taxable_amount: totSubtotal,
                    ppn: local_ppn,
                    currency_code: currency_code,
                    rate: footRate,
                    local_total : local,
                    total: total 

               },
               dataType: "json",
               success: function (response) {
                    if (response.code == 200) {
                         alert("Data SO berhasil disimpan");
                         window.location="<?php echo base_url() ?>sales_order";
                    }
               }
          });
     }

</script>