<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_setting extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('level') == '') {
		   redirect('login');
		}
		$data = array(
		   'judul_page' => "Company Setting",
		   'konten' => 'company/setting',
		);
		$this->load->view('v_index', $data);
	}

	public function update()
	{
		$format_so = $this->input->post('format_so');

		$segments = explode("/", $format_so);
		if (sizeof($segments) > 1) {
			// code...
		} else {
			$this->session->set_flashdata('message', message('danger','Format tidak dikenali, harus menggunakan pemisah "/" '));
        	redirect('Company_setting','refresh');
        	exit;
		}

		$f1 = substr($segments[1], 0,2);
		$f2= substr($segments[1], 2,2);
		if ($f1 == 'yy' OR $f1 == 'MM') {
			
		} elseif($f2 == 'yy' OR $f2 == 'MM') {

		} else {
			$this->session->set_flashdata('message', message('danger','Format tanggal tidak dikenali'));
        	redirect('Company_setting','refresh');
        	exit;
		}

		$data = array(
			'company_id' => $this->input->post('company_id'),
			'company_name' => $this->input->post('company_name'),
			'alamat' => $this->input->post('alamat'),
			'format_so' => $format_so,
			'logo' => $retVal = ($_FILES['foto']['name'] == '') ? $_POST['foto_old'] : upload_gambar_biasa('logo', 'image/logo/', 'jpeg|png|jpg|gif', 10000, 'foto')
		);

		$this->db->where('id', 1);
		$this->db->update('company_setting', $data);

		$this->session->set_flashdata('message', message('success','Data berhasil diupdate'));
        redirect('Company_setting','refresh');

	}

}

/* End of file Company_setting.php */
/* Location: ./application/controllers/Company_setting.php */