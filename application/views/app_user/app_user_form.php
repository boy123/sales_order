<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
     <div class="card border-top border-0 border-4 border-info">
          <div class="card-body">
               <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                         <div><i class="bx bx-edit-alt me-1 font-22 text-info"></i>
                         </div>
                         <h5 class="mb-0 text-info"><?php echo $judul_form ?></h5>
                    </div>
                    <hr />
                    <div class="row">
                         <div class="col-md-8">
                              <div class="row mb-3">
                                   <label for="nama_lengkap" class="col-sm-3
                                   col-form-label">Nama Lengkap
                                        <?php echo form_error('nama_lengkap') ?></label>
                                   <div class="col-sm-9">
                                        <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap"
                                             placeholder="Nama Lengkap" value="<?php echo $nama_lengkap; ?>" />
                                   </div>
                              </div>
                              <div class="row mb-3">
                                   <label for="username" class="col-sm-3
                                   col-form-label">Username
                                        <?php echo form_error('username') ?></label>
                                   <div class="col-sm-9">
                                        <input type="text" class="form-control" name="username" id="username"
                                             placeholder="Username" value="<?php echo $username; ?>" />
                                   </div>
                              </div>
                              <div class="row mb-3">
                                   <label for="password" class="col-sm-3
                                   col-form-label">Password
                                        <?php echo form_error('password') ?></label>
                                   <div class="col-sm-9">
                                        <input type="text" class="form-control" name="password" id="password"
                                             placeholder="Password" />
                                        <?php if(!empty($password)): ?>
                                             <p>*) Kosongkan password jika tidak diisi.</p>
                                             <input type="hidden" name="password_old" value="<?php echo $password ?>">
                                        <?php endif ?>
                                        
                                   </div>
                              </div>
                              <div class="row mb-3">
                                   <label for="level" class="col-sm-3
                                   col-form-label">Level
                                        <?php echo form_error('level') ?></label>
                                   <div class="col-sm-9">
                                        <select name="level" id="level" onchange="setLevel()" class="single-select">
                                             <option value="<?php echo $level ?>"><?php echo $level ?></option>
                                             <option value="sales">sales</option>
                                             <option value="other">other</option>
                                             <option value="superadmin">superadmin</option>
                                        </select>
                                   </div>
                              </div>
                              <div class="row mb-3">
                                   <label for="username" class="col-sm-3
                                   col-form-label">ACC NO</label>
                                   <div class="col-sm-9">
                                        <select name="acc_no" id="acc_no" class="single-select form-control" onchange="cekAccNo()">
                                             <?php if ($this->uri->segment(2) == 'update'): ?>
                                                  <option value="<?php echo $acc_no ?>"><?php echo $acc_no ?></option>
                                             <?php else: ?>
                                                  <option value="">Pilih</option>
                                             <?php endif ?>
                                             
                                             <?php 
                                             $this->db->select('AccNo,CompanyName');
                                                  foreach ($this->db->get('debtor')->result() as $rw): ?>
                                                       <option value="<?php echo $rw->AccNo ?>"><?php echo $rw->AccNo.' | '.$rw->CompanyName ?></option>
                                             <?php endforeach ?>
                                        </select>
                                   </div>
                              </div>

                              <div class="row mb-3">
                                   <label for="username" class="col-sm-3
                                   col-form-label">Branch</label>
                                   <div class="col-sm-9">
                                        <select name="branch" id="branch" class="single-select" style="width: 100%">
                                             <?php if ($this->uri->segment(2) == 'update'): ?>
                                                  <option value="<?php echo $branch ?>"><?php echo $branch ?></option>
                                             <?php else: ?>
                                                  <option value="">Pilih</option>
                                             <?php endif ?>
                                                  
                                        </select>
                                   </div>
                              </div>

                              <div class="row mb-3">
                                   <label for="level" class="col-sm-3
                                   col-form-label">Status</label>
                                   <div class="col-sm-9">
                                        <select name="aktif" id="aktif" class="single-select" required>
                                             <option value="<?php echo $aktif ?>"><?php echo ($aktif == 'y') ? 'Aktif' : 'Tidak Aktif' ?></option>
                                             <option value="y">Aktif</option>
                                             <option value="t">Tidak Aktif</option>
                                        </select>
                                   </div>
                              </div>

                              <div class="row mb-3">
                                   <label for="username" class="col-sm-3
                                   col-form-label">Sales Agent</label>
                                   <div class="col-sm-9">

                                        <?php 

                                        foreach ($this->db->get('sales_agent')->result() as $rw):
                                             $checked = "";

                                             if ($this->uri->segment(2) == 'update') {
                                                  $id_user = $this->uri->segment(3);
                                                  $sales_agent = $this->db->query("SELECT * FROM user_agent WHERE id_user='$id_user' and sales_agent='$rw->SalesAgent' ");
                                                  if ($sales_agent->num_rows() > 0) {
                                                       $checked = "checked";
                                                  }
                                             }
                                         ?>
                                        <div class="form-check">
                                         <input type="checkbox" class="form-check-input" name="sales_agent[]" value="<?php echo $rw->SalesAgent ?>" <?php echo $checked ?>>
                                         <label class="form-check-label" for="check1"><?php echo $rw->SalesAgent ?></label>
                                       </div>

                                       <?php endforeach ?>
                                   </div>
                              </div>

                              
                         </div>
                         <div class="col-md-4">
                              <div class="row mb-3">

                                   <div class="row mb-1">
                                        <div class="col-sm-12">
                                             <?php if ($foto == ''): ?>
                                                  <a href="image/no_image.png">
                                                       <img src="image/no_image.png" style="width: 100%;">
                                                  </a>
                                             <?php else: ?>
                                                  <a href="image/user/<?php echo $foto ?>">
                                                       <img src="image/user/<?php echo $foto ?>" style="width: 100%;">
                                                  </a>
                                             <?php endif ?>
                                        </div>
                                   </div>
                                   <div class="row mb-3">
                                        <div class="col-sm-12">
                                             <input class="form-control" type="file" name="foto" id="formFile">
                                             <input type="hidden" name="foto_old" value="<?php echo $foto ?>">
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>

                    <input type="hidden" name="id_user" value="<?php echo $id_user; ?>" />
                    <button type="submit" class="btn btn-primary"><i class="bx bx-save"></i>
                         <?php echo $button ?></button>
                    <a href="<?php echo site_url('app_user') ?>" class="btn btn-outline-info"><i class="bx bx-exit"></i>
                         Cancel</a>

               </div>
          </div>
     </div>
</form>

<script type="text/javascript">

     function setLevel() {
          level = $("#level").val();
          if (level == 'other') {
               $("#acc_no").removeAttr('disabled');
               $("#branch").removeAttr('disabled');
               $("[type=checkbox]").attr('disabled', 'disabled');
               $("[type=submit]").attr('disabled', 'disabled');
          } else if (level == 'sales') {
               $("[type=submit]").removeAttr('disabled');
               $("[type=checkbox]").removeAttr('disabled');
               $("#acc_no").attr('disabled', 'disabled');
               $("#branch").attr('disabled', 'disabled');
          }
     }

     function cekAccNo() {
          AccNo = $('#acc_no').val();
          $.ajax({
               type: "GET",
               url: "sales_order/cekAccNo/"+AccNo,
               dataType: "json",
               success: function (response) {
                    $("[type=submit]").removeAttr('disabled');
                    getBranch(AccNo);
               }
          });
     }
     function getBranch(AccNo) {
          $.ajax({
               type: "GET",
               url: "sales_order/getBranch/"+AccNo,
               dataType: "html",
               success: function (response) {
                    $("#branch").html(response);
               }
          });
     }
</script>