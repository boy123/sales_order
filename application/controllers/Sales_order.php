<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_order extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('level') == '') {
		    redirect('login');
		}
		$data = array(
		   'judul_page' => "Sales Order",
		   'konten' => 'sales_order/view',
		);
		$this->load->view('v_index', $data);
	}

	public function add()
	{
		if ($this->session->userdata('level') == '') {
		    redirect('login');
		}
		$data = array(
		   'judul_page' => "Input Sales Order",
		   'konten' => 'sales_order/add',
		);
		$this->cart->destroy();
		$this->load->view('v_index', $data);
	}

	public function edit()
	{
		if ($this->session->userdata('level') == '') {
		    redirect('login');
		}
		$data = array(
		   'judul_page' => "Update Sales Order",
		   'konten' => 'sales_order/edit',
		);
		$this->cart->destroy();
		$so_h = $this->db->get_where('so_header', ['so_no' => $this->input->get('so_no') ])->row();
		$so_d = $this->db->get_where('so_detail', ['so_no' => $this->input->get('so_no') ])->result();
		foreach ($so_d as $rw) {
			$dt = array(
				'id' => $rw->item_code,
				'qty' => $rw->qty,
				'price'   => 1,
	        	'name'    => 'as',
				'options' => array(
					'uom'=> $rw->uom,
					'unit_price' => ($rw->unit_price == '') ? 0 : $rw->unit_price,
					'discount' => ($rw->discount == '') ? 0 : $rw->discount,
					'subtotal' => ($rw->subtotal == '') ? 0 : $rw->subtotal,
					'ppn_code' => ($rw->ppn_code == '') ? 0 : $rw->ppn_code,
					'ppn_rate' => ($rw->ppn_rate == '') ? 0 : $rw->ppn_rate,
					'ppn_amount' => ($rw->ppn_amount == '') ? 0 : $rw->ppn_amount,
					'proj_no' => ($rw->proj_no == '') ? '-' : $rw->proj_no,
					'AccNo'=> $so_h->acc_no 
				)
			);
			$this->cart->insert($dt);
		}

		$this->load->view('v_index', $data);
	}

	public function view()
	{
		if ($this->session->userdata('level') == '') {
		    redirect('login');
		}
		$data = array(
		   'judul_page' => "View Sales Order",
		   'konten' => 'sales_order/detail',
		);
		$this->cart->destroy();
		$so_h = $this->db->get_where('so_header', ['so_no' => $this->input->get('so_no') ])->row();
		$so_d = $this->db->get_where('so_detail', ['so_no' => $this->input->get('so_no') ])->result();
		foreach ($so_d as $rw) {
			$dt = array(
				'id' => $rw->item_code,
				'qty' => $rw->qty,
				'price'   => 1,
	        	'name'    => 'as',
				'options' => array(
					'uom'=> $rw->uom, 
					'unit_price' => ($rw->unit_price == '') ? 0 : $rw->unit_price,
					'discount' => ($rw->discount == '') ? 0 : $rw->discount,
					'subtotal' => ($rw->subtotal == '') ? 0 : $rw->subtotal,
					'ppn_code' => ($rw->ppn_code == '') ? 0 : $rw->ppn_code,
					'ppn_rate' => ($rw->ppn_rate == '') ? 0 : $rw->ppn_rate,
					'ppn_amount' => ($rw->ppn_amount == '') ? 0 : $rw->ppn_amount,
					'proj_no' => ($rw->proj_no == '') ? '-' : $rw->proj_no,
					'AccNo'=> $so_h->acc_no 
				)
			);
			$this->cart->insert($dt);
		}

		$this->load->view('v_index', $data);
	}

	public function cancel()
	{
		$this->cart->destroy();
		redirect('sales_order/add','refresh');
	}

	public function cekAccNo($AccNo)
	{
		$this->db->where('AccNo', $AccNo);
		$debtor = $this->db->get('debtor')->row();

		$result = array(
			'CompanyName' => $debtor->CompanyName,
			'Address1' => $debtor->Address1,
			'Address2' => $debtor->Address2,
			'Address3' => $debtor->Address3,
			'Address4' => $debtor->Address4,
			'DeliverAddr1' => $debtor->DeliverAddr1,
			'DeliverAddr2' => $debtor->DeliverAddr2,
			'DeliverAddr3' => $debtor->DeliverAddr3,
			'DeliverAddr4' => $debtor->DeliverAddr4,
			'DisplayTerm' => $debtor->DisplayTerm,
			'SalesAgent' => $debtor->SalesAgent
		);

		echo json_encode($result);
	}

	public function getBranch($AccNo)
	{
		$this->db->where('AccNo', $AccNo);
		$branch = $this->db->get('branch');

		foreach ($branch->result() as $rw) {
			?>
			<option value="<?php echo $rw->BranchCode ?>"><?php echo $rw->BranchName ?></option>
			<?php
		}
	}

	public function cekUOM($ItemCode)
	{
		$this->db->where('ItemCode', $ItemCode);
		$uom = $this->db->get('item_uom');
		echo '<option value="">Pilih</option>';
		foreach ($uom->result() as $rw) {
			?>
			<option value="<?php echo $rw->UOM ?>"><?php echo $rw->UOM ?></option>
			<?php
		}
	}

	public function saveItem()
	{
		$uom = $this->input->post('uom');//implode(",", $this->input->post('uom'));
		$AccNo = $this->input->post('acc_no');

		$ItemCode = $this->input->post('item_code');
		// $uom = $rw['options']['uom'];
		// $AccNo = $rw['options']['AccNo'];
		$unit_price = 0;
		$discount = 0;


		$debtor = $this->db->query("SELECT PriceCategory FROM debtor where PriceCategory !='' and AccNo='$AccNo' ");
		if ($debtor->num_rows() > 0) {
			$this->db->where('ItemCode', $ItemCode);
			$this->db->where('UOM', $uom);
			$item_price = $this->db->get('item_price');
			if ($item_price->num_rows() > 0) {
				$unit_price = (int) $item_price->row()->FixedPrice;
				$discount = $item_price->row()->FixedDetailDiscount;
			} else {
				$this->db->where('ItemCode', $ItemCode);
				$this->db->where('UOM', $uom);
				$item_uom = $this->db->get('item_uom')->row();
				if ($item_uom->Price != '') {
					$unit_price = $item_uom->Price;
				}
			}
		} else {
			$this->db->where('ItemCode', $ItemCode);
			$this->db->where('UOM', $uom);
			$item_uom = $this->db->get('item_uom')->row();
			if ($item_uom->Price != '') {
				$unit_price = $item_uom->Price;
			}
		}

		
		$subtotal = ($this->input->post('qty') * $unit_price) - $discount ;
		$ppn_code = get_data('debtor','AccNo',$AccNo, 'TaxType');
		$ppn_rate = (int) get_data('tax','TaxType',$ppn_code,'TaxRate');
		$ppn_amount = $ppn_rate * $subtotal;

		$data = array(
			'id' => $this->input->post('item_code'),
			'qty' => $this->input->post('qty'),
			'price'   => 1,
        	'name'    => 'as',
			'options' => array(
				'uom'=> $uom,
				'unit_price' => $unit_price,
				'discount' => $discount,
				'subtotal' => $subtotal,
				'ppn_code' => $ppn_code,
				'ppn_rate' => $ppn_rate,
				'ppn_amount' => $ppn_amount,
				'proj_no' => '',
				'AccNo'=> $AccNo 
			)
		);
		$this->cart->insert($data);

		echo json_encode(array(
			'code' => 200,
			'msg' => 'success',
			'result' => $this->cart->contents()
		));
	}

	public function saveSO($status)
	{	
		$so_no = so_no($_POST['date']);
		$_POST['so_no'] = $so_no;
		$_POST['status'] = $status;
		$_POST['created_at'] = get_waktu();
		$_POST['created_by'] = $this->session->userdata('id_user');
		$this->db->insert('so_header', $_POST);

		foreach ($this->cart->contents() as $rw) {
			$this->db->insert('so_detail', array(
				'so_no'=> $so_no,
				'item_code' => $rw['id'],
				'qty' => $rw['qty'],
				'uom' => $rw['options']['uom'],
				'unit_price' => $rw['options']['unit_price'],
				'discount' => $rw['options']['discount'],
				'subtotal' => $rw['options']['subtotal'],
				'ppn_code' => $rw['options']['ppn_code'],
				'ppn_rate' => $rw['options']['ppn_rate'],
				'ppn_amount' => $rw['options']['ppn_amount'],
				'proj_no' => $rw['options']['proj_no'],
			));
		}

		$this->cart->destroy();
		echo json_encode(array(
			'code' => 200,
			'msg' => 'success',
			'result' => $this->cart->contents()
		));

	}

	public function updateSO($status)
	{	
		$so_no = $this->input->get('so_no');
		$_POST['status'] = $status;
		$_POST['updated_at'] = get_waktu();
		$_POST['updated_by'] = $this->session->userdata('id_user');
		$this->db->where('so_no', $so_no);
		$this->db->update('so_header', $_POST);

		$this->db->where('so_no', $so_no);
		$this->db->delete('so_detail');

		foreach ($this->cart->contents() as $rw) {
			$this->db->insert('so_detail', array(
				'so_no'=> $so_no,
				'item_code' => $rw['id'],
				'qty' => $rw['qty'],
				'uom' => $rw['options']['uom'],
				'unit_price' => $rw['options']['unit_price'],
				'discount' => $rw['options']['discount'],
				'subtotal' => $rw['options']['subtotal'],
				'ppn_code' => $rw['options']['ppn_code'],
				'ppn_rate' => $rw['options']['ppn_rate'],
				'ppn_amount' => $rw['options']['ppn_amount'],
				'proj_no' => $rw['options']['proj_no'],
			));
		}

		$this->cart->destroy();
		echo json_encode(array(
			'code' => 200,
			'msg' => 'success',
			'result' => $this->cart->contents()
		));

	}

	public function getListItemAdd()
	{
		$result = array();
		foreach ($this->cart->contents() as $rw) {

			$ItemCode = $rw['id'];
			$uom = $rw['options']['uom'];
			$AccNo = $rw['options']['AccNo'];
			$unit_price = 0;
			$discount = 0;


			$debtor = $this->db->query("SELECT PriceCategory FROM debtor where PriceCategory !='' and AccNo='$AccNo' ");
			if ($debtor->num_rows() > 0) {
				$this->db->where('ItemCode', $ItemCode);
				$this->db->where('UOM', $uom);
				$item_price = $this->db->get('item_price');
				if ($item_price->num_rows() > 0) {
					$unit_price = (int) $item_price->row()->FixedPrice;
					$discount = $item_price->row()->FixedDetailDiscount;
				} else {
					$this->db->where('ItemCode', $ItemCode);
					$this->db->where('UOM', $uom);
					$item_uom = $this->db->get('item_uom')->row();
					if ($item_uom->Price != '') {
						$unit_price = $item_uom->Price;
					}
				}
			} else {
				$this->db->where('ItemCode', $ItemCode);
				$this->db->where('UOM', $uom);
				$item_uom = $this->db->get('item_uom')->row();
				if ($item_uom->Price != '') {
					$unit_price = $item_uom->Price;
				}
			}

			
			$subtotal = ($rw['qty'] * $unit_price) - $discount ;
			$ppn_code = get_data('debtor','AccNo',$AccNo, 'TaxType');
			if (isset($_GET['so_no'])) {
				$so_no = $this->input->get('so_no');
				$ppn_rate = (int) get_data('so_detail','so_no',$so_no,'ppn_rate');
			} else {
				$ppn_rate = (int) get_data('tax','TaxType',$ppn_code,'TaxRate');
			}
			
			$ppn_amount = $ppn_rate * $subtotal;

			array_push($result, array(
				'rowid' => $rw['rowid'],
				'item_code' => $ItemCode,
				'description' => get_data('item','ItemCode',$rw['id'],'Description'),
				'qty' => $rw['qty'],
				'uom' => $uom,
				'unit_price' => $unit_price,
				'discount' => $discount,
				'subtotal' => $subtotal,
				'ppn_code' => $ppn_code,
				'ppn_rate' => $ppn_rate,
				'ppn_amount' => $ppn_amount,
				'proj_no' => ''
			));
		}

		echo json_encode(array(
			'code' => 200,
			'msg' => 'success',
			'result' => $result
		));
	}

	public function getListItem()
	{
		$result = array();
		foreach ($this->cart->contents() as $rw) {

			// $ItemCode = $rw['id'];
			// $uom = $rw['options']['uom'];
			// $AccNo = $rw['options']['AccNo'];
			// $unit_price = 0;
			// $discount = 0;


			// $debtor = $this->db->query("SELECT PriceCategory FROM debtor where PriceCategory !='' and AccNo='$AccNo' ");
			// if ($debtor->num_rows() > 0) {
			// 	$this->db->where('ItemCode', $ItemCode);
			// 	$this->db->where('UOM', $uom);
			// 	$item_price = $this->db->get('item_price');
			// 	if ($item_price->num_rows() > 0) {
			// 		$unit_price = (int) $item_price->row()->FixedPrice;
			// 		$discount = $item_price->row()->FixedDetailDiscount;
			// 	} else {
			// 		$this->db->where('ItemCode', $ItemCode);
			// 		$this->db->where('UOM', $uom);
			// 		$item_uom = $this->db->get('item_uom')->row();
			// 		if ($item_uom->Price != '') {
			// 			$unit_price = $item_uom->Price;
			// 		}
			// 	}
			// } else {
			// 	$this->db->where('ItemCode', $ItemCode);
			// 	$this->db->where('UOM', $uom);
			// 	$item_uom = $this->db->get('item_uom')->row();
			// 	if ($item_uom->Price != '') {
			// 		$unit_price = $item_uom->Price;
			// 	}
			// }

			
			// $subtotal = ($rw['qty'] * $unit_price) - $discount ;
			// $ppn_code = get_data('debtor','AccNo',$AccNo, 'TaxType');
			// $ppn_rate = (int) get_data('tax','TaxType',$ppn_code,'TaxRate');
			// $ppn_amount = $ppn_rate * $subtotal;

			// array_push($result, array(
			// 	'rowid' => $rw['rowid'],
			// 	'item_code' => $ItemCode,
			// 	'description' => get_data('item','ItemCode',$rw['id'],'Description'),
			// 	'qty' => $rw['qty'],
			// 	'uom' => $uom,
			// 	'unit_price' => $unit_price,
			// 	'discount' => $discount,
			// 	'subtotal' => $subtotal,
			// 	'ppn_code' => $ppn_code,
			// 	'ppn_rate' => $ppn_rate,
			// 	'ppn_amount' => $ppn_amount,
			// 	'proj_no' => ''
			// ));

			$ItemCode = $rw['id'];
			$uom = $rw['options']['uom'];
			$AccNo = $rw['options']['AccNo'];

			array_push($result, array(
				'rowid' => $rw['rowid'],
				'item_code' => $ItemCode,
				'description' => get_data('item','ItemCode',$rw['id'],'Description'),
				'qty' => $rw['qty'],
				'uom' => $uom,
				'unit_price' => (int) $rw['options']['unit_price'],
				'discount' => (int) $rw['options']['discount'],
				'subtotal' => (int) $rw['options']['subtotal'],
				'ppn_code' => $rw['options']['ppn_code'],
				'ppn_rate' => (float) $rw['options']['ppn_rate'],
				'ppn_amount' =>(int) $rw['options']['ppn_amount'],
				'proj_no' => $rw['options']['proj_no']
			));
		}

		echo json_encode(array(
			'code' => 200,
			'msg' => 'success',
			'result' => $result
		));
	}
	
	public function removeItem($rowid)
	{
		$data = array(
	        'rowid'  => $rowid,
	        'qty'    => 0,
		);
		$this->cart->update($data);
		echo json_encode(array(
			'code' => 200,
			'msg' => 'success'
		));
	}

	public function getCurrency($AccNo)
	{
		$this->db->where('AccNo', $AccNo);
		$debtor = $this->db->get('debtor')->row();
		$currency_code = $debtor->CurrencyCode;
		$BankSellRate = (int) get_data('currency','CurrencyCode',$currency_code,'BankSellRate');
		echo json_encode(array(
			'currency_code' => $currency_code,
			'rate' => $BankSellRate
		));
	}

	public function deleteSO()
	{
		$so_no = $this->input->get('so_no');
		$this->db->where('so_no', $so_no);
		$this->db->delete('so_header');

		$this->db->where('so_no', $so_no);
		$this->db->delete('so_detail');

		$this->session->set_flashdata('message', message('success','Data berhasil dihapus'));
        redirect('sales_order','refresh');
	}

}

/* End of file Sales_order.php */
/* Location: ./application/controllers/Sales_order.php */