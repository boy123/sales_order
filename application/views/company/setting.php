<?php 
$company = $this->db->get('company_setting')->row();
 ?>
<div class="card">
     <div class="card-body">
          <div class="row">
               <div class="row mb-3">
                    <div class="col">
                         <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
               </div>
               <form action="company_setting/update" method="POST" enctype="multipart/form-data">
                    <div class="row mb-1">
                         <div class="col-sm-3">
                              <?php if ($company->logo == ''): ?>
                                   <a href="image/no_image.png">
                                        <img src="image/no_image.png" style="width: 100%;">
                                   </a>
                              <?php else: ?>
                                   <a href="image/logo/<?php echo $company->logo ?>">
                                        <img src="image/logo/<?php echo $company->logo ?>" style="width: 100%;">
                                   </a>
                              <?php endif ?>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <div class="col-sm-3">
                              <input class="form-control" type="file" name="foto" id="formFile">
                              <input type="hidden" name="foto_old" value="<?php echo $company->logo ?>">
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label class="col-sm-3 col-form-label">Company ID</label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="company_id" id="company_id" placeholder="Company ID" value="<?php echo $company->company_id; ?>" required/>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label class="col-sm-3 col-form-label">Company Name</label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name" value="<?php echo $company->company_name; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label class="col-sm-3 col-form-label">Address</label>
                         <div class="col-sm-9">
                              <textarea class="form-control" name="alamat" id="alamat" placeholder="Address" required><?php echo $company->alamat ?></textarea>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label class="col-sm-3 col-form-label">Format No SO</label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="format_so" id="format_so" value="<?php echo $company->format_so; ?>" required/>
                         </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Update</button>
               </form>
          </div>
     </div>
</div>