<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="<?php echo base_url() ?>">
     <!--favicon-->
     <link rel="icon" href="assets/logo.png" type="image/png" />
	<!-- loader-->
	<link href="assets/css/pace.min.css" rel="stylesheet" />
	<script src="assets/js/pace.min.js"></script>
	<!-- Bootstrap CSS -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/bootstrap-extended.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&amp;display=swap" rel="stylesheet">
	<link href="assets/css/app.css" rel="stylesheet">
	<link href="assets/css/icons.css" rel="stylesheet">
	<title>Sales Order - Reset Password</title>
</head>

<body>
	<!-- wrapper -->
	<div class="wrapper">
		<div class="authentication-reset-password d-flex align-items-center justify-content-center">
			<div class="row">
				<div class="col-12 col-lg-10 mx-auto">
					<div class="card">
						<div class="row g-0">
							<div class="col-lg-5 border-end">
								<div class="card-body">
									<div class="p-5">
										<div class="text-start">
											<!-- <img src="assets/images/logo-img.png" width="180" alt=""> -->
										</div>
										<h4 class="mt-5 font-weight-bold">Reset Password</h4>
										<!-- <p class="text-muted">We received your reset password request. Please enter your new password!</p> -->
										<form action="" method="POST">
										<div class="mb-3 mt-5">
											<label class="form-label">Username</label>
											<input type="text" name="username" class="form-control" placeholder="Masukkan Username" />
										</div>
										<div class="mb-3">
											<label class="form-label">Password Baru</label>
											<input type="text" name="password" class="form-control" placeholder="Password Baru" />
										</div>
										<div class="d-grid gap-2">
											<button type="submit" class="btn btn-primary">Ubah Password</button> <a href="login" class="btn btn-light"><i class='bx bx-arrow-back mr-1'></i>Back to Login</a>
										</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<img src="assets/images/login-images/forgot-password-frent-img.jpg" class="card-img login-img h-100" alt="...">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end wrapper -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
     <script type="text/javascript"><?php echo $this->session->userdata('pesan') ?></script>
</body>
</html>