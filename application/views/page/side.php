<div class="sidebar-wrapper" data-simplebar="true">
     <div class="sidebar-header">
          <div>
               <!-- <img src="assets/logo.png" style="widht: 60px;height: 30px;" alt="logo icon"> -->
               <span>APPS</span>
          </div>
          <div>
               <!-- <h4 class="logo-text" style="color: #CC9C27">Zam Zam</h4> -->
          </div>
          <div class="toggle-icon ms-auto" style="color: #CC9C27"><i class='bx bx-arrow-to-left'></i>
          </div>
     </div>
     <!--navigation-->
     <ul class="metismenu" id="menu">
          <li class="menu-label">Main Menu</li>
          <li>
               <a href="app">
                    <div class="parent-icon"><i class='bx bx-home-circle'></i>
                    </div>
                    <div class="menu-title">Dashboard</div>
               </a>
          </li>
          <li>
               <a href="sales_order">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Sales Order</div>
               </a>
          </li>
          
		
		<?php if($this->session->userdata('level') == 'superadmin'): ?>

          <li>
               <a href="company_setting">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Setting Company</div>
               </a>
          </li>
		<li>
               <a href="app_user">
                    <div class="parent-icon"><i class='bx bx-user-circle'></i>
                    </div>
                    <div class="menu-title">Master User</div>
               </a>
          </li>
          <?php endif ?>
     </ul>
     <!--end navigation-->
</div>