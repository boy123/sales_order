<div class="card">
     <div class="card-body">
          <div class="row mb-4">
               <div class="col">
                    <a href="sales_order/add" class="btn btn-primary"><i class="bx bx-plus mr-1"></i>Add</a>
               </div>
          </div>
          <div class="row mb-3">
               <div class="col">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
               </div>
          </div>
          <div class="table-responsive">
               <table id="exampleDataTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                         <tr>
                              <th>No</th>
                              <th>Doc No</th>
                              <th>Date</th>
                              <th>Company Name</th>
                              <th>Subtotal</th>
                              <th>PPN</th>
                              <th>Total</th>
                              <th>Ref</th>
                              <th>Created User</th>
                              <th>Created Time</th>
                              <th>Status</th>
                              <th>Action</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php 
                         $no = 1;

                         $id_user = $this->session->userdata('id_user');
                         $acc_no = get_data('app_user','id_user',$id_user,'acc_no');
                         if ($this->session->userdata('level') == 'sales') {
                              $where = "WHERE sales_agent IN (select sales_agent from user_agent where id_user='$id_user') ";
                         } elseif($this->session->userdata('level') == 'other') {
                              $where = "WHERE acc_no = '$acc_no' ";
                         } else {
                              $where = "";
                         }
                         $sql = "SELECT * FROM so_header $where ORDER BY date DESC";
                         $sales_order = $this->db->query($sql);

                         // $this->db->order_by('date', 'desc');
                         // $sales_order = $this->db->get('so_header');
                         
                         foreach ($sales_order->result() as $so): ?>
                              <tr>
                                   <td><?php echo $no ?></td>
                                   <td><?php echo $so->so_no ?></td>
                                   <td><?php echo $so->date ?></td>
                                   <td><?php echo get_data('debtor','AccNo',$so->acc_no,'CompanyName') ?></td>
                                   <td class="text-end"><?php echo number_format($so->subtotal,2) ?></td>
                                   <td class="text-end"><?php echo number_format($so->ppn,2) ?></td>
                                   <td class="text-end"><?php echo number_format($so->total,2) ?></td>
                                   <td><?php echo $so->ref_doc ?></td>
                                   <td><?php echo get_data('app_user','id_user',$so->created_by,'nama_lengkap') ?></td>
                                   <td><?php echo $so->created_at ?></td>
                                   <td>
                                        <b><?php echo $so->status ?></b>
                                   </td>
                                   <td>
                                        <a href="sales_order/view?so_no=<?php echo $so->so_no ?>" title="Lihat Data" class="btn btn-sm btn-info">
                                             <i class="bx bx-check-square me-0"></i>
                                        </a>

                                        <?php if ($so->status == 'draft'): ?>
                                        
                                        <a href="sales_order/edit?so_no=<?php echo $so->so_no ?>" title="Edit Data" class="btn btn-sm btn-success">
                                             <i class="bx bx-edit-alt me-0"></i>
                                        </a>

                                        <a href="sales_order/deleteSO?so_no=<?php echo $so->so_no ?>" title="Hapus Data" onclick="javasciprt: return confirm('Yakin akan hapus data ini ?')" class="btn btn-sm btn-danger">
                                             <i class="bx bx-trash-alt me-0"></i>
                                        </a>

                                        <?php endif ?>

                                        
                                   </td>
                              </tr>
                         <?php $no++; endforeach ?>
                    </tbody>
               </table>
          </div>
     </div>
</div>